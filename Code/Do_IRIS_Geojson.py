#!/usr/bin/env python
# coding: utf-8

# In[1]:


from shapely import wkt
from shapely.geometry import Polygon
import geopandas 
import pandas as pd
import sqlite3 as sq

def do_iris_geojson():
    conn = sq.connect('./Code/dpe.db')
    cur = conn.cursor()
    filtre_iris = pd.read_sql('select * from filtre_iris', conn)
    conn.commit()
    conn.close()
    gdf = geopandas.GeoDataFrame(filtre_iris, crs="EPSG:4326")
    gdf['geometry'] = gdf['geometry'].apply(wkt.loads)
    df_geo = geopandas.GeoDataFrame(gdf, geometry = 'geometry')
    df_geo.set_geometry('geometry', inplace=True)
    df_geo.to_file('iris.json', driver="GeoJSON")
            


# In[ ]:





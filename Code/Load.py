#!/usr/bin/env python
# coding: utf-8

# In[1]:


import re
import sqlite3
import pandas
import datetime
import json
import fiona
import geopandas as gpd
import shapely


"""
injecter les raws datas dans la base de données
    load_dpe()
    load_communes()
    load_iris()
"""

def load_dpe():
    """
    injecter dpe raw data dans la base de données
    
    """
    conn = sqlite3.connect("./Code/dpe.db")
    cur = conn.cursor()
    with open('dpe.csv') as dpe_file:
        dpe_df = pandas.read_csv(dpe_file)
    cur.execute("""DROP TABLE IF EXISTS raw_dpe""")
    dpe_df.to_sql("raw_dpe", conn)
    conn.commit()
    cur.close()
    conn.close()
    print("Fin du chargement des données brutes de dpe en base de données")

def load_communes():
    """
    injecter communes raw data dans la base de données
    
    """
    conn = sqlite3.connect("./Code/dpe.db")
    cur = conn.cursor()
    with open("communes.json", "r") as communes_file:
        communes_data = json.load(communes_file)
        cur.execute("""DROP TABLE IF EXISTS raw_communes""")
        cur.execute(
            """CREATE TABLE IF NOT EXISTS raw_communes(
            geojson TEXT,
            code_insee TEXT,
            nom_commune TEXT,
            trigram TEXT,
            code_postal TEXT
            )"""
        )
        for commune_data in communes_data.get('features'):
            com_geojson = json.dumps({
                'type': 'Feature',
                'geometry': commune_data.get('geometry').get('coordinates'),
            })
            com_code_insee = commune_data.get('properties').get('code_insee')
            com_nom = commune_data.get('properties').get('nom')
            com_trigram = commune_data.get('properties').get('trigram')
            com_code_postal = commune_data.get('properties').get('code_postal')
            cur.execute(
                """INSERT INTO raw_communes
                (geojson, code_insee, nom_commune, trigram, code_postal)
                VALUES (?, ?, ?, ?, ?)""",
                (com_geojson, com_code_insee, com_nom, com_trigram, com_code_postal)
            )
    conn.commit()
    cur.close()
    conn.close()
    print("Fin du chargement des données brutes des communes en base de données ")
    
def load_iris_nb_logements():
    """
    injecter le nombre de logements IRIS raw data dans database
    
    """
    conn = sqlite3.connect("./Code/dpe.db")
    cur = conn.cursor()
    with open('base-ic-logement-2017.CSV') as iris_nb_logements_file:
        iris_nb_logements_df = pandas.read_csv(iris_nb_logements_file, delimiter=';')
    cur.execute("""DROP TABLE IF EXISTS raw_iris_nb_logements""")
    iris_nb_logements_df.to_sql("raw_iris_nb_logements", conn)
    conn.commit()
    cur.close()
    conn.close()
    print("Fin du chargementdes données du nombre de logements par maille IRIS en base de données")

def load_contours_iris():
    
    """
        injecter contours IRIS raw data dans la base de données
        
    """

    conn = sqlite3.connect("./Code/dpe.db")
    cur = conn.cursor()
    cur.execute("""DROP TABLE IF EXISTS raw_iris""")

    contours_iris = gpd.read_file("IRIS-GE_2-0__SHP_LAMB93_D038_2020-01-01/IRIS-GE/"             "1_DONNEES_LIVRAISON_2020-07-00352/IRIS-GE_2-0_SHP_LAMB93_D038-2020/IRIS_GE.SHP")
    contours_iris['geometry'] = contours_iris.apply(lambda x: shapely.wkt.dumps(x.geometry), axis=1)
    contours_iris.to_sql('raw_iris', conn, if_exists='replace', index=False) # writes the pd.df to SQLIte DB
 
    conn.commit()
    cur.close()
    conn.close()
    print("Fin du chargement des données brutes des contours IRIS en base de données")

    
load_dpe()
load_communes()
load_iris_nb_logements()
load_contours_iris()
    


# In[ ]:





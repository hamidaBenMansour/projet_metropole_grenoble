#!/usr/bin/env python
# coding: utf-8

# In[ ]:


import pandas as pd
import sqlite3


try:
    import  nom_commune
    
except ModuleNotFoundError:
    
    from Code import nom_commune
    
    
commune=nom_commune.nom_commune()

def filtre():
    conn = sqlite3.connect('./Code/dpe.db')
    cur = conn.cursor()
    
    df = pd.read_sql('select * from dpe_corrigé_com', conn)
    df['commune'] = df['commune'].str.lower()
    df = df[df['commune'].isin(commune)]
    df = df.reset_index()
    df = df.drop(['index'], axis=1)
    df.to_csv("dpe_Metropole.csv")
    return df
    conn.commit()
    cur.close()
    conn.close()


#!/usr/bin/env python
# coding: utf-8

# In[9]:


import sqlite3
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.ticker as mtick
from matplotlib import ticker
from matplotlib.colors import ListedColormap
import numpy as np

def graph():
    
    conn = sqlite3.connect('./Code/dpe.db')
    cur = conn.cursor()
    
    query = "SELECT  [classe_estimation_ges], [classe_consommation_energie] FROM dpe_filtré"
    graph = pd.read_sql(query, conn)
           
    
    n = graph[graph['classe_consommation_energie'] == 'N' ].index
    graph.drop(n, inplace=True)
    m = graph[graph['classe_estimation_ges'] == 'N' ].index
    graph.drop(m, inplace=True)
    #colors = ('red', 'blue', 'lightgreen', 'gray', 'cyan')
    cmap = ListedColormap(["green","lawngreen",  "GreenYellow", "yellow", "gold", "orange", "red"])

    #cmap = ListedColormap(['#929591', '#7fc97f', '#f0027f', '#ff9999','#66b3ff','#99ff99'])
    ax = graph.groupby(["classe_consommation_energie", "classe_estimation_ges"])["classe_estimation_ges"].size().groupby(level=0).apply(
            (lambda x: (100 * x / x.sum()).round(2))
    ).unstack()
    fig=pd.DataFrame(ax)
    fig=fig.fillna(0)
    fag=fig.plot(kind='barh',stacked=True, figsize=(30,10), mark_right = True, xlabel="% de repartition", colormap=cmap, fontsize=20)
    df_total = fig.sum(axis=1)
    df_rel = fig[fig.columns[0:]].div(df_total, 0)*100
    
    for n in df_rel:
        for i, (cs, ab, pc, tot) in enumerate(zip(fig.iloc[:, :].cumsum(1)[n], fig[n], df_rel[n], df_total)):
            plt.text(tot, i, "    "+str(int(tot))+ "%",  rotation=1, fontsize=10)
            if pc > 1:
                plt.text(np.round(cs - ab/2), i, str(int(np.round(pc,0))) + '%', va='center', ha='center', rotation=1, fontsize=20)
    
    fag.legend([x[5:].capitalize() for x in fig.columns[0:]], bbox_to_anchor=(0, 1), loc='upper left')

    fag.set_ylabel ("Classe énergie", fontsize=20 )
    fag.set_xlabel ("Pourcentage", fontsize=20)
    fag.text(105, 6.5, "Classe GES", fontsize=20)
    fag.xaxis.set_visible(False)
    plt.legend(bbox_to_anchor=(1,1,0,0), fontsize=20)
    #plt.title("Proportion des classes de consommation énergétique par classe d'estimation d'émissions de gaz à effet de serre", fontsize=30)
    plt.gca().xaxis.set_major_formatter(ticker.PercentFormatter())
    plt.show()
    
    conn.commit()
    cur.close()
    conn.close()
    
  


# In[ ]:





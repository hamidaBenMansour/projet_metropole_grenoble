#!/usr/bin/env python
# coding: utf-8

# In[26]:



import Filtre_dpe_38
import pandas as pd
import folium
import folium.plugins
import numpy as np




df=Filtre_dpe_38.filtre(dff)
def carto(df):
    fg1=folium.FeatureGroup(name='classe_ce')
    classement_dpe = df["classe_consommation_energie"].unique()
    classe = []
    coordonnees = []

    for elm in classement_dpe:
        classe.append(elm)
    classe.sort()
    classe.pop()

    carte = folium.Map(location=(45.188529, 5.724524), zoom_start=11)

    for elm in classe:
        carto = df.loc[df['classe_consommation_energie'] == elm]
        carto = carto.dropna(subset = ["latitude"], inplace=False)
        carto = carto.dropna(subset = ["longitude"], inplace=False)
        coordonnees = []
        for _, row in carto.iterrows():
            coordonnees.append([row["latitude"],row["longitude"]])
        folium.plugins.MarkerCluster(
            control=False,
            name=elm,
            locations=coordonnees,
            highlight=True,
        ).add_to(fg1)

    fg2=folium.FeatureGroup(name='classe_ges')
    classement_dpe = df["classe_estimation_ges"].unique()
    classe = []
    coordonnees = []

    for elm in classement_dpe:
        classe.append(elm)
    classe.sort()
    classe.pop()

    carte = folium.Map(location=(45.188529, 5.724524), zoom_start=11)

    for elm in classe:
        carto = df.loc[df['classe_estimation_ges'] == elm]
        carto = carto.dropna(subset = ["latitude"], inplace=False)
        carto = carto.dropna(subset = ["longitude"], inplace=False)
        coordonnees = []
        for _, row in carto.iterrows():
            coordonnees.append([row["latitude"],row["longitude"]])
        plugins.MarkerCluster(
            control=True,
            name=elm,
            locations=coordonnees,
            highlight=True
        ).add_to(fg2)       
    fg1.add_to(carte)
    fg2.add_to(carte)
    folium.LayerControl().add_to(carte)
    return carte


# In[ ]:





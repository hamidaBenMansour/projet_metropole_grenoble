#!/usr/bin/env python
# coding: utf-8

# In[1]:


import pandas as pd
import csv

def corrig_nom_commune():

    df = pd.read_csv('dpe.csv')

    df.loc[df.commune == 'St barthelemy de sechilien', 'commune'] = 'saint-barthélemy-de-séchilienne'
    df.loc[df.commune == 'Saint Barthelémy de Séchilienne', 'commune'] = 'saint-barthélemy-de-séchilienne'
    df.loc[df.commune == 'Saint-Barthélemy-de-Séchilienne', 'commune'] = 'saint-barthélemy-de-séchilienne'
    df.loc[df.commune == 'Saint-barthelemy-de-sechilienne', 'commune'] = 'saint-barthélemy-de-séchilienne'
    df.loc[df.commune == 'SAINT-BARTHÉLEMY-DE-SÉCHILIENNE', 'commune'] = 'saint-barthélemy-de-séchilienne'
    df.loc[df.commune == 'ST BARTHELEMY SECHILIENNE', 'commune'] = 'saint-barthélemy-de-séchilienne'
    df.loc[df.commune == 'SAINT BARTHELEMY DE SECHILIENNE', 'commune'] = 'saint-barthélemy-de-séchilienne'
    df.loc[df.commune == 'SAINT-BARTHELEMY-DE-SECHILIENNE', 'commune'] = 'saint-barthélemy-de-séchilienne'
    df.loc[df.commune == 'ST BARTHELEMY DE SECHILIENNE', 'commune'] = 'saint-barthélemy-de-séchilienne'

    df.loc[df.commune == 'Saint-égrève', 'commune'] = 'saint-égrève'
    df.loc[df.commune == 'SAINT EGRÈVE', 'commune'] = 'saint-égrève'
    df.loc[df.commune == 'SAINT-ÉGRÈVE', 'commune'] = 'saint-égrève'
    df.loc[df.commune == 'SANT EGREVE', 'commune'] = 'saint-égrève'
    df.loc[df.commune == 'ST  EGREVE', 'commune'] = 'saint-égrève'
    df.loc[df.commune == 'ST-EGREVE', 'commune'] = 'saint-égrève'
    df.loc[df.commune == 'ST EGREVE', 'commune'] = 'saint-égrève'
    df.loc[df.commune == 'SAINT EGREVE', 'commune'] = 'saint-égrève'
    df.loc[df.commune == 'SAINT-EGREVE', 'commune'] = 'saint-égrève'
    df.loc[df.commune == 'Saint Egreve', 'commune'] = 'saint-égrève'
    df.loc[df.commune == 'St Egreve', 'commune'] = 'saint-égrève'
    df.loc[df.commune == 'St egreve', 'commune'] = 'saint-égrève'
    df.loc[df.commune == 'Saint-Egrève', 'commune'] = 'saint-égrève'
    df.loc[df.commune == 'Saint-Égrève', 'commune'] = 'saint-égrève'
    df.loc[df.commune == 'saint-égrève', 'commune'] = 'saint-égrève'
    df.loc[df.commune == 'St Egrève', 'commune'] = 'saint-égrève'
    df.loc[df.commune == 'Saint-??grève', 'commune'] = 'saint-égrève'
    df.loc[df.commune == 'Saint Egrève', 'commune'] = 'saint-égrève'


    df.loc[df.commune == 'Saint-Georges-de-Commiers', 'commune'] = 'saint-georges-de-commiers'
    df.loc[df.commune == 'saint-georges-de-commiers', 'commune'] = 'saint-georges-de-commiers'
    df.loc[df.commune == 'SAINT-GEORGES-DE-COMMIERS', 'commune'] = 'saint-georges-de-commiers'
    df.loc[df.commune == 'ST GEORGES DE COMMIERS', 'commune'] = 'saint-georges-de-commiers'
    df.loc[df.commune == 'SAINT GEORGES DE COMMIER', 'commune'] = 'saint-georges-de-commiers'
    df.loc[df.commune == 'SAINT GEORGES DE COMMIERS', 'commune'] = 'saint-georges-de-commiers'
    df.loc[df.commune == 'SAINT GEORGE DE COMMIERS', 'commune'] = 'saint-georges-de-commiers'


    df.loc[df.commune == 'HAUTE-JARRIE', 'commune'] = 'jarrie'
    df.loc[df.commune == 'HAUTE JARRIE', 'commune'] = 'jarrie'

    df.loc[df.commune == 'Noyaret', 'commune'] = 'noyarey'
    df.loc[df.commune == 'Noyarey', 'commune'] = 'noyarey'
    df.loc[df.commune == 'NOYAREY', 'commune'] = 'noyarey'
    df.loc[df.commune == 'NOYAREY ', 'commune'] = 'noyarey'

    df.loc[df.commune == 'Poisat', 'commune'] = 'poisat'
    df.loc[df.commune == 'POISAT', 'commune'] = 'poisat'

    df.loc[df.commune == 'QUAIX EN CHARTREUSE ', 'commune'] = 'quaix-en-chartreuse'
    df.loc[df.commune == 'Quaix en Chartreuse', 'commune'] = 'quaix-en-chartreuse'
    df.loc[df.commune == 'Quaix-en-Chartreuse', 'commune'] = 'quaix-en-chartreuse'
    df.loc[df.commune == 'QUAIX-EN-CHARTREUSE', 'commune'] = 'quaix-en-chartreuse'
    df.loc[df.commune == 'QUAIX EN CHARTREUSE', 'commune'] = 'quaix-en-chartreuse'



    df.loc[df.commune == 'MEYLAN', 'commune'] = 'meylan'
    df.loc[df.commune == 'MEYLAN ', 'commune'] = 'meylan'
    df.loc[df.commune == 'MEYLANS', 'commune'] = 'meylan'
    df.loc[df.commune == 'Meylan ', 'commune'] = 'meylan'


    df.loc[df.commune == 'ECHIROLLES', 'commune'] = 'échirolles'
    df.loc[df.commune == 'ÉCHIROLLES', 'commune'] = 'échirolles'
    df.loc[df.commune == 'ECHIROLLES ', 'commune'] = 'échirolles'
    df.loc[df.commune == 'ECHIROLLES CEDEX', 'commune'] = 'échirolles'
    df.loc[df.commune == 'Echirolles', 'commune'] = 'échirolles'

    df.loc[df.commune == 'EYBENS ', 'commune'] = 'eybens'
    df.loc[df.commune == 'Eybens ', 'commune'] = 'eybens'

    df.loc[df.commune == 'CHAMP-SUR-DRAC', 'commune'] = 'champ-sur-drac'
    df.loc[df.commune == 'CHAMP SUR DRAC', 'commune'] = 'champ-sur-drac'
    df.loc[df.commune == 'CHAMPS SUR DRAC', 'commune'] = 'champ-sur-drac'
    df.loc[df.commune == 'Champ sur Drac', 'commune'] = 'champ-sur-drac'


    df.loc[df.commune == 'BRIÉ-ET-ANGONNES', 'commune'] = 'brié-et-angonnes'
    df.loc[df.commune == 'BRIE ET ANGONNES', 'commune'] = 'brié-et-angonnes'
    df.loc[df.commune == 'BRIE-ET-ANGONNES', 'commune'] = 'brié-et-angonnes'
    df.loc[df.commune == 'Brié et Angonnes', 'commune'] = 'brié-et-angonnes'


    df.loc[df.commune == 'FONTANIL-CORNILLON', 'commune'] = 'le fontanil-cornillon' 
    df.loc[df.commune == 'FONTANIL CORNILLON', 'commune'] = 'le fontanil-cornillon'
    df.loc[df.commune == 'LE FONTANIL-CORNILLON', 'commune'] = 'le fontanil-cornillon'
    df.loc[df.commune == 'LE FONTANIL CORNILLON', 'commune'] = 'le fontanil-cornillon'
    df.loc[df.commune == 'LE FONTANILE CORNILLON', 'commune'] = 'le fontanil-cornillon'
    df.loc[df.commune == 'Fontanil Cornillon', 'commune'] = 'le fontanil-cornillon'
    df.loc[df.commune == 'Fontanil-Cornillon', 'commune'] = 'le fontanil-cornillon'
    df.loc[df.commune == 'Le Fontanil Cornillon', 'commune'] = 'le fontanil-cornillon'
    df.loc[df.commune == 'Fontanil cornillon', 'commune'] = 'le fontanil-cornillon'


    df.loc[df.commune == 'LE PONT-DE-CLAIX', 'commune'] = 'le pont-de-claix'
    df.loc[df.commune == 'PONT DE CLAIX', 'commune'] = 'le pont-de-claix'
    df.loc[df.commune == 'LE PONT DE CLAIX', 'commune'] = 'le pont-de-claix'
    df.loc[df.commune == 'LE-PONT-DE-CLAIX', 'commune'] = 'le pont-de-claix'
    df.loc[df.commune == 'PONT DE CLAIX (LE)', 'commune'] = 'le pont-de-claix'
    df.loc[df.commune == 'LE PONT-DE-ClAIX', 'commune'] = 'le pont-de-claix'
    df.loc[df.commune == 'LEPONT-DE-CLAIX ', 'commune'] = 'le pont-de-claix'
    df.loc[df.commune == 'Le Pont de Claix', 'commune'] = 'le pont-de-claix'
    df.loc[df.commune == 'Le pont de claix', 'commune'] = 'le pont-de-claix'
    df.loc[df.commune == 'Le pont de Claix', 'commune'] = 'le pont-de-claix'
    df.loc[df.commune == 'Pont de Claix', 'commune'] = 'le pont-de-claix'

    df.loc[df.commune == 'GRENOBLE ', 'commune'] = 'grenoble'
    df.loc[df.commune == 'GRENOBLE 1', 'commune'] = 'grenoble'
    df.loc[df.commune == 'RENOBLE', 'commune'] = 'grenoble'
    df.loc[df.commune == 'GRENOBLEA', 'commune'] = 'grenoble'
    df.loc[df.commune == 'GRENOBLE.', 'commune'] = 'grenoble'
    df.loc[df.commune == 'GENOBLE', 'commune'] = 'grenoble'
    df.loc[df.commune == 'Grenoble n°101', 'commune'] = 'grenoble'
    df.loc[df.commune == 'Grenoble (38000)', 'commune'] = 'grenoble'
    df.loc[df.commune == 'Gernoble', 'commune'] = 'grenoble'

    df.loc[df.commune == 'MONT-SAINT-MARTIN', 'commune'] = 'mont-saint-martin' 
    df.loc[df.commune == 'MONT ST MARTIN', 'commune'] = 'mont-saint-martin'
    df.loc[df.commune == 'MONT SAINT MARTIN', 'commune'] = 'mont-saint-martin'


    df.loc[df.commune == 'Notre-Dame-de-Commiers', 'commune'] = 'notre-dame-de-commiers' 
    df.loc[df.commune == 'NOTRE-DAME-DE-COMMIERS', 'commune'] = 'notre-dame-de-commiers'
    df.loc[df.commune == 'NOTRE DAME DE COMMIERS', 'commune'] = 'notre-dame-de-commiers'
    df.loc[df.commune == 'NOTRE DAME DE COMMIER', 'commune'] = 'notre-dame-de-commiers'

    df.loc[df.commune == 'MONTCHABOU', 'commune'] = 'montchaboud'
    df.loc[df.commune == 'MONTCHABOUD', 'commune'] = 'montchaboud'
    df.loc[df.commune == 'MONTCHABOUT', 'commune'] = 'montchaboud'

    df.loc[df.commune == 'PRELENFREY DU GUA', 'commune'] = 'le gua'
    df.loc[df.commune == 'LE GUA', 'commune'] = 'le gua'
    df.loc[df.commune == 'SAINT BARTHELEMY DU GUA', 'commune'] = 'le gua'
    df.loc[df.commune == 'GUA', 'commune'] = 'le gua'
    df.loc[df.commune == 'LA GUA', 'commune'] = 'le gua'
    df.loc[df.commune == 'LE GUA PRELENFREY', 'commune'] = 'le gua'


    df.loc[df.commune == 'MIRIBEL-LANCHÂTRE', 'commune'] = 'miribel-lanchâtre'
    df.loc[df.commune == 'MIRIBEL LANCHATRE', 'commune'] = 'miribel-lanchâtre'
    df.loc[df.commune == 'MIRIBEL-LANCHATRE', 'commune'] = 'miribel-lanchâtre'

    df.loc[df.commune == 'Notre-Dame-de-Mésage', 'commune'] = 'notre-dame-de-mésage'
    df.loc[df.commune == 'NOTRE-DAME-DE-MÉSAGE', 'commune'] = 'notre-dame-de-mésage'
    df.loc[df.commune == 'NOTRE DAME DE MESAGE', 'commune'] = 'notre-dame-de-mésage'
    df.loc[df.commune == 'NOTRE-DAME-DE-MESAGE', 'commune'] = 'notre-dame-de-mésage'


    df.loc[df.commune == 'St martin le vinoux', 'commune'] = 'saint-martin-le-vinoux'
    df.loc[df.commune == 'Saint Le Martin Vinoux', 'commune'] = 'saint-martin-le-vinoux'
    df.loc[df.commune == 'Saint Martin Le Vinoux', 'commune'] = 'saint-martin-le-vinoux'
    df.loc[df.commune == 'Saint Martin le Vinoux', 'commune'] = 'saint-martin-le-vinoux'
    df.loc[df.commune == 'Saint-Martin-le-Vinoux', 'commune'] = 'saint-martin-le-vinoux'
    df.loc[df.commune == 'St Martin le Vinoux', 'commune'] = 'saint-martin-le-vinoux'
    df.loc[df.commune == 'ST MARTIN LE VINOUX', 'commune'] = 'saint-martin-le-vinoux'
    df.loc[df.commune == 'SAINT-MARTIN-LE-VINOUX', 'commune'] = 'saint-martin-le-vinoux'
    df.loc[df.commune == 'SAINT MARTIN LE VINOUX', 'commune'] = 'saint-martin-le-vinoux'
    df.loc[df.commune == 'ST-MARTIN-LE-VINOUX', 'commune'] = 'saint-martin-le-vinoux'

    df.loc[df.commune == 'Saint Paul de Varces', 'commune'] = 'saint-paul-de-varces'
    df.loc[df.commune == 'SAINT PAUL DE VARCES', 'commune'] = 'saint-paul-de-varces'
    df.loc[df.commune == 'ST-PAUL-DE-VARCES', 'commune'] = 'saint-paul-de-varces'
    df.loc[df.commune == 'ST PAUL DE VARCES', 'commune'] = 'saint-paul-de-varces'
    df.loc[df.commune == 'SAINT-PAUL-DE-VARCES', 'commune'] = 'saint-paul-de-varces'


    df.loc[df.commune == 'SASSENAGE', 'commune'] = 'sassenage'

    df.loc[df.commune == 'Saint-Pierre-de-Mésage', 'commune'] = 'saint-pierre-de-mésage'
    df.loc[df.commune == 'SAINT-PIERRE-DE-MÉSAGE', 'commune'] = 'saint-pierre-de-mésage'
    df.loc[df.commune == 'SAINT PIERRE DE MESAGE', 'commune'] = 'saint-pierre-de-mésage'
    df.loc[df.commune == 'ST PIERRE DE MESAGE', 'commune'] = 'saint-pierre-de-mésage'
    df.loc[df.commune == 'SAINT -PIERRE- DE-MÉSAGE', 'commune'] = 'saint-pierre-de-mésage'
    df.loc[df.commune == 'SAINT-PIERRE-DE-MESAGE', 'commune'] = 'saint-pierre-de-mésage'


    df.loc[df.commune == "SAINT MARTIN D'HÈRES", 'commune'] = "saint-martin-d'hères"
    df.loc[df.commune == "SAINT-MARTIN-D'HÈRES", 'commune'] = "saint-martin-d'hères"
    df.loc[df.commune == 'saint martin d heres', 'commune'] = "saint-martin-d'hères"
    df.loc[df.commune == 'St Martin d(Heres', 'commune'] = "saint-martin-d'hères"
    df.loc[df.commune == "saint martin d'heres", 'commune'] = "saint-martin-d'hères"
    df.loc[df.commune == "St Martn D'heres", 'commune'] = "saint-martin-d'hères"
    df.loc[df.commune == "Saint-Martin-D'Heres", 'commune'] = "saint-martin-d'hères"
    df.loc[df.commune == 'st martin d heres', 'commune'] = "saint-martin-d'hères"
    df.loc[df.commune == "St martin d'heres", 'commune'] = "saint-martin-d'hères"
    df.loc[df.commune == "Saint martin d'hères", 'commune'] = "saint-martin-d'hères"
    df.loc[df.commune == "Saint Matin d'Hères", 'commune'] = "saint-martin-d'hères"
    df.loc[df.commune == "Saint Martin d'Hère", 'commune'] = "saint-martin-d'hères"
    df.loc[df.commune == "Saint Martin d'Hères", 'commune'] = "saint-martin-d'hères"
    df.loc[df.commune == "St Martin d'Hères", 'commune'] = "saint-martin-d'hères"
    df.loc[df.commune == "Saint Martin D'Hères", 'commune'] = "saint-martin-d'hères"
    df.loc[df.commune == "Saint Martin d'hères", 'commune'] = "saint-martin-d'hères"
    df.loc[df.commune == "Saint-Martin-d'Hères", 'commune'] = "saint-martin-d'hères"
    df.loc[df.commune == "ST MARRTIN D'HERES", 'commune'] = "saint-martin-d'hères"
    df.loc[df.commune == 'SAINT-MARTIN-D HERES', 'commune'] = "saint-martin-d'hères"
    df.loc[df.commune == 'ST-MARTIN-D-HERES', 'commune'] = "saint-martin-d'hères"
    df.loc[df.commune == 'ST MARTIN D HERES ', 'commune'] = "saint-martin-d'hères"
    df.loc[df.commune == "ST MARTIN D'HERES", 'commune'] = "saint-martin-d'hères"
    df.loc[df.commune == "SAINT MARTIN D'HERES", 'commune'] = "saint-martin-d'hères"
    df.loc[df.commune == 'SAINT MARTIN D HERES', 'commune'] = "saint-martin-d'hères"
    df.loc[df.commune == 'ST MARTIN D HERES', 'commune'] = "saint-martin-d'hères"
    df.loc[df.commune == "SAINT-MARTIN-D'HERES", 'commune'] = "saint-martin-d'hères"

    df.loc[df.commune == 'LE SAPPEY-EN-CHARTREUSE', 'commune'] = 'le sappey-en-chartreuse'
    df.loc[df.commune == 'LE SAPPEY EN CHARTREUSE', 'commune'] = 'le sappey-en-chartreuse'
    df.loc[df.commune == 'SAPPEY EN CHARTREUSE', 'commune'] = 'le sappey-en-chartreuse'
    df.loc[df.commune == 'LE SAPPEY EN CHARTEUSE', 'commune'] = 'le sappey-en-chartreuse'

    df.loc[df.commune == 'Sechilienne', 'commune'] = 'séchilienne'
    df.loc[df.commune == 'Séchilienne', 'commune'] = 'séchilienne'
    df.loc[df.commune == 'SÉCHILIENNE', 'commune'] = 'séchilienne'
    df.loc[df.commune == 'SECHILIENNE', 'commune'] = 'séchilienne'

    df.loc[df.commune == 'Seyssins', 'commune'] = 'seyssins'
    df.loc[df.commune == 'SEYSSINS', 'commune'] = 'seyssins'


    df.loc[df.commune == 'La tronche', 'commune'] = 'la tronche'
    df.loc[df.commune == 'La Tronche', 'commune'] = 'la tronche'
    df.loc[df.commune == 'LA TRONCHE', 'commune'] = 'la tronche'
    df.loc[df.commune == 'LATRONCHE CEDEX', 'commune'] = 'la tronche'
    df.loc[df.commune == 'LA-TRONCHE', 'commune'] = 'la tronche'
    df.loc[df.commune == 'TRONCHE', 'commune'] = 'la tronche'
    df.loc[df.commune == 'TRONCHE (LA) ', 'commune'] = 'la tronche'


    df.loc[df.commune == 'SEYSSINET-PARISET', 'commune'] = 'seyssinet-pariset'
    df.loc[df.commune == 'SEYSSINET PARISET', 'commune'] = 'seyssinet-pariset'
    df.loc[df.commune == 'SEYSSINET-PARISET CEDEX', 'commune'] = 'seyssinet-pariset'
    df.loc[df.commune == 'SEYSSINET', 'commune'] = 'seyssinet-pariset'
    df.loc[df.commune == 'Seyssinet pariset', 'commune'] = 'seyssinet-pariset'
    df.loc[df.commune == 'Seyssinet', 'commune'] = 'seyssinet-pariset'
    df.loc[df.commune == 'Seyssinet Pariset', 'commune'] = 'seyssinet-pariset'
    df.loc[df.commune == 'Seyssinet-Pariset', 'commune'] = 'seyssinet-pariset'


    df.loc[df.commune == 'DOMENE ', 'commune'] = 'domène'
    df.loc[df.commune == 'DOMENE', 'commune'] = 'domène'
    df.loc[df.commune == 'Domene', 'commune'] = 'domène'
    df.loc[df.commune == 'Domène', 'commune'] = 'domène'
    df.loc[df.commune == 'DOMÈNE', 'commune'] = 'domène'

    df.loc[df.commune == 'Varces-Alliéres et Risset', 'commune'] = 'varces-allières-et-risset'
    df.loc[df.commune == 'Varces Allieres et Risset', 'commune'] = 'varces-allières-et-risset'
    df.loc[df.commune == 'Varces-Allières-et-Risset', 'commune'] = 'varces-allières-et-risset'
    df.loc[df.commune == 'VARCES-ALLIÈRES-ET-RISSET', 'commune'] = 'varces-allières-et-risset'
    df.loc[df.commune == 'VARCES-ALLIERES-ET-RISSET', 'commune'] = 'varces-allières-et-risset'
    df.loc[df.commune == 'VARCES ALLIERES ET RISSET', 'commune'] = 'varces-allières-et-risset'
    df.loc[df.commune == 'VARCES-ALLIÈRES-ET-RISSET CEDEX', 'commune'] = 'varces-allières-et-risset'
    df.loc[df.commune == 'VARCES-ALLIERES ET RISSET', 'commune'] = 'varces-allières-et-risset'

    df.loc[df.commune == 'VAULNAVEYS LE BAS', 'commune'] = 'vaulnaveys-le-bas'
    df.loc[df.commune == 'VAULNAVEYS-LE-BAS', 'commune'] = 'vaulnaveys-le-bas'
    df.loc[df.commune == 'Vaulnaveys-le-Bas', 'commune'] = 'vaulnaveys-le-bas'
    df.loc[df.commune == 'VAULNAVEYS  LE BAS', 'commune'] = 'vaulnaveys-le-bas'

    df.loc[df.commune == 'Vaulnaveys-le-Haut', 'commune'] = 'vaulnaveys-le-haut'
    df.loc[df.commune == '38410 VAULNAVEYS LE HAUT', 'commune'] = 'vaulnaveys-le-haut'
    df.loc[df.commune == 'VAULNAVEY-LE-HAUT', 'commune'] = 'vaulnaveys-le-haut'
    df.loc[df.commune == 'VAULNAVEYS-LE-HAUT', 'commune'] = 'vaulnaveys-le-haut'
    df.loc[df.commune == 'VAULNAVEYS LE HAUT', 'commune'] = 'vaulnaveys-le-haut'
    df.loc[df.commune == 'Vaulnaveys Le Haut', 'commune'] = 'vaulnaveys-le-haut'
    df.loc[df.commune == 'Vaulnaveys le haut', 'commune'] = 'vaulnaveys-le-haut'


    df.loc[df.commune == 'VEUREY VOROIZE', 'commune'] = 'veurey-voroize'
    df.loc[df.commune == 'VEUREY-VOROIZE', 'commune'] = 'veurey-voroize'

    df.loc[df.commune == 'VIZILLE', 'commune'] = 'vizille'
    df.loc[df.commune == 'LE PEAGE DE VIZILLE', 'commune'] = 'vizille'
    df.loc[df.commune == 'PEAGE DE VIZILLE', 'commune'] = 'vizille'
    df.loc[df.commune == 'PEAGE-DE -VIZILLE', 'commune'] = 'vizille'
    df.loc[df.commune == 'VIZILLE ( péage)', 'commune'] = 'vizille'
    df.loc[df.commune == 'LE PEAGE DE VIZILLE', 'commune'] = 'vizille'
    df.loc[df.commune == 'VIZILLE', 'commune'] = 'vizille'
    
    df.to_csv("dpe_corrigé.csv")
    
    return (len(df))


# In[ ]:





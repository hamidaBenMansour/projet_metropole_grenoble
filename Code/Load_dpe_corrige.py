#!/usr/bin/env python
# coding: utf-8

# In[3]:


import sqlite3
import pandas
def load_dpe_corrigé():
    """
    injecter dpe_38 data dans la base de données
    
    """
    conn = sqlite3.connect("./Code/dpe.db")
    cur = conn.cursor()
    with open('dpe_corrigé.csv') as dpe_file:
        dpe_df = pandas.read_csv(dpe_file)
    cur.execute("""DROP TABLE IF EXISTS dpe_corrigé_com""")
    dpe_df.to_sql("dpe_corrigé_com", conn)
    conn.commit()
    cur.close()
    conn.close()
    print("Fin du chargement des données brutes de dpe en base de données")


# In[ ]:





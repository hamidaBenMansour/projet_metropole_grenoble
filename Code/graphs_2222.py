#!/usr/bin/env python
# coding: utf-8

# In[204]:


import sqlite3
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.ticker as mtick
from matplotlib import ticker
from matplotlib.colors import ListedColormap
import seaborn as sns
get_ipython().run_line_magic('matplotlib', 'inline')
plt.style.use('ggplot')
sns.set_style("white")

def graph_1():
    
    conn = sqlite3.connect('./Code/dpe.db')
    cur = conn.cursor()
    query = "SELECT * FROM dpe_filtré;"
    df = pd.read_sql(query, conn)

    n = df[df['classe_consommation_energie'] == 'N' ].index
    df.drop(n, inplace=True)
   

    cmap = ListedColormap(["green","lawngreen", "GreenYellow", "yellow", "gold", "orange", "red"])
    
    ax=df.groupby(['classe_consommation_energie'])['classe_consommation_energie'].value_counts().unstack().plot(kind='barh', figsize=(30,10), stacked=True,  ylabel="COUNT", colormap=cmap,  fontsize=20)

    totals = []
    for i in ax.patches:
        totals.append(i.get_width())
    total = sum(totals)
    
    for i, p  in enumerate(ax.patches):
        if p.get_width()>0:
            ax.text(p.get_width(), p.get_y(), str(round((p.get_width()))), fontsize=15, fontweight='bold')
            
    plt.ylabel("Classe Dpe", fontsize=20)
    plt.xlabel ("Nombre de DPE par consommation énergétique", fontsize=20)
    plt.legend(bbox_to_anchor=(1,1,0,0),  fontsize=20)
    ax.set_title("Répartition des DPE par étiquette d'énergie", fontsize=30)
    
    conn.commit()
    cur.close()
    conn.close()
    
    
    
def graph_2():
    
    conn = sqlite3.connect('./Code/dpe.db')
    cur = conn.cursor()
    query = "SELECT * FROM dpe_filtré;"
    df = pd.read_sql(query, conn)

    m = df[df['classe_estimation_ges'] == 'N' ].index
    df.drop(m, inplace=True)
    
    cma = ListedColormap(["Thistle","Plum", "Violet","Magenta", "Orchid", "MediumOrchid",  "purple"])

    ax=df.groupby(['classe_estimation_ges'])['classe_estimation_ges'].value_counts().unstack().plot.barh(figsize=(30,10), stacked=True,  ylabel="COUNT", colormap=cma,  fontsize=20)
    #for p in ax.patches:
    
    totals = []
    for i in ax.patches:
        totals.append(i.get_width())
    total = sum(totals)
    
    for i, p  in enumerate(ax.patches):
        if p.get_width()>0:
            ax.text(p.get_width(), p.get_y(), str(round((p.get_width()))), fontsize=15, fontweight='bold')
        
    plt.ylabel("Classe Dpe", fontsize=20)
    plt.xlabel ("Nombre de DPE par émission de gaz à effet de serre", fontsize=20)
    plt.legend(bbox_to_anchor=(1,1,0,0),  fontsize=20)
    ax.set_title("Répartition des DPE par étiquette de climat", fontsize=30)
   
   
    
    conn.commit()
    cur.close()
    conn.close()


# In[ ]:





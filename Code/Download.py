#!/usr/bin/env python
# coding: utf-8

# In[2]:


import pandas as pd
import requests
import urllib
import zipfile
import py7zr
import os
import gzip
import xlrd




def download_dpe():
    '''
    Téléchargement dpe.csv
    
    '''
    url_dpe = "https://koumoul.com/s/data-fair/api/v1/datasets/dpe-38/raw"
    dpe = requests.get(url_dpe)
    with open("dpe.csv", "w") as file:
        file.write(dpe.text)
        if str(dpe.status_code)[0] == "2":
            print()
        else:
            print("Un problème est survenu durant le téléchargement.")
    df = pd.read_csv("dpe.csv")        
    return df



def download_json():
    '''
    Téléchargement communes.json
    
    '''
    url_communes = "https://entrepot.metropolegrenoble.fr/opendata/Metro/LimitesCommunales/json/LIMITES_COMMUNALES_METRO_EPSG4326.json"
    communes = requests.get(url_communes)
    with open("communes.json", "w") as file:
        file.write(communes.text)
        if str(communes.status_code)[0] == "2":
            print()
        else:
            print("Un problème est survenu durant le téléchargement.")
            
            
            
def download_cont_iris():
    
    '''
    Permet de télecharger contours_iris.7z et extraire IRIS-GE_2-0__SHP_LAMB93_D038_2020-01-01
    
    '''
    url_contours_iris = "ftp://Iris_GE_ext:eeLoow1gohS1Oot9@ftp3.ign.fr/IRIS-GE_2-0__SHP_LAMB93_D038_2020-01-01.7z.001" # 38
    contours_iris = urllib.request.urlretrieve(url_contours_iris, "contours_iris.7z")
    if os.path.isfile("contours_iris.7z"):
        if os.path.getsize("contours_iris.7z") > 1500000:
            print()
        else:
            print("Un problème est survenu durant le téléchargement.")
    else:
        print("Un problème est survenu durant le téléchargement")
      

    if os.path.isfile("contours_iris.7z"):
        archive = py7zr.SevenZipFile('contours_iris.7z', mode='r')
        archive.extractall(path="./")
        archive.close()
        if os.path.isdir("IRIS-GE_2-0__SHP_LAMB93_D038_2020-01-01"):
            print()
        else:
            print("Un problème est survenu durant la décompression.")
    else:
        print("L'archive nécessaire n'est pas présente")     


        
def download_log_communes():
    
    r = requests.get("https://entrepot.metropolegrenoble.fr/opendata/Metro/LimitesCommunales/json/LIMITES_COMMUNALES_METRO_EPSG4326.json")
    data = r.json()
    nombre_commune = len(data["features"])
    code = []
    
    for i in range(nombre_commune):
        code.append(data["features"][i]["properties"]["code_insee"])
    df_dp = pd.read_csv("dpe_Metropole.csv") 
    
    
    df = pd.read_excel ('insee_rp_hist_1968.xlsx', engine='openpyxl')
    df.rename(columns={'Observatoire des territoires - ANCT': 'codgeo', 'Unnamed: 1': 'libgeo', 'Unnamed: 2': 'an', 'Unnamed: 3': 'Nombre total de logements'}, inplace=True)
    df.drop(df.index[0:4],0,inplace=True)
    df_2017 = df[df["an"] == "2017"]
    df_2017.shape
    
    Df_Metro_logement_2017=df_2017[df_2017['codgeo'].isin(code)]
    Df_Metro_logement_2017.reset_index(level=0, inplace=True)
    
    dff= Df_Metro_logement_2017.drop(["index"], axis=1)
    dff['libgeo'] = dff['libgeo'].str.lower()
    dff.rename(columns={'libgeo': 'commune'}, inplace=True)
    dff.loc[dff.commune == 'fontanil-cornillon', 'commune'] = 'le fontanil-cornillon'


    dfff=df_dp[['commune', 'id']]
    df_metrique2 = pd.DataFrame(dfff.groupby(['commune']).count()) 
    df_metrique2.reset_index(level=0, inplace=True)
    df_metrique2.rename(columns={'id': 'Nombre de logements contrôlés'}, inplace=True)

    logement=dff.join(df_metrique2.set_index('commune'), on='commune')
    
    
    return (logement)       
        
        
            
        
def download_log_IRIS():
    """
    Télechargement base-ic-logement-2017_csv.zip et extraire base-ic-logement-2017.CSV 
    
    """
    url_nombre_logements_iris = "https://www.insee.fr/fr/statistiques/fichier/4799305/base-ic-logement-2017_csv.zip"
    nombre_logements_iris = requests.get(url_nombre_logements_iris)
    
    with open("base-ic-logement-2017_csv.zip", "wb") as file:
        file.write(nombre_logements_iris.content)
        if str(nombre_logements_iris.status_code)[0] == "2":
            print()
        else:
            print("Un problème est survenu durant le téléchargement.")
        

    with zipfile.ZipFile("base-ic-logement-2017_csv.zip", 'r') as zip_ref:
        zip_ref.extractall("./")
        if os.path.isfile("base-ic-logement-2017.CSV"):
            print()
        else: 
            print("Un problème est survenu durant la décompression.")
            
            

download_dpe()        
download_json()
download_cont_iris()
download_log_communes()
download_log_IRIS()


# In[ ]:





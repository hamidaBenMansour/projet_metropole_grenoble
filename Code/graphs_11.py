#!/usr/bin/env python
# coding: utf-8

# In[ ]:





# In[1]:


import sqlite3
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.ticker as mtick
from matplotlib import ticker
from matplotlib.colors import ListedColormap


def graph_log():
    conn = sqlite3.connect('./Code/dpe.db')
    cur = conn.cursor()
    
    query = "SELECT [tr002_type_batiment_libelle] FROM dpe_filtré;"
    df = pd.read_sql(query, conn)
    explode = (0, 0.2, 0)  
    labels = ['Maison', 'Appartement', 'Logements collectifs']
    colors = ['#ff9999','#66b3ff','#99ff99']
    
    data=df.groupby(['tr002_type_batiment_libelle']).size().reset_index(name='counts')

    fig1, ax1 = plt.subplots()
    ax1.pie(data['counts'], explode=explode, labels=labels, autopct='%1.1f%%', shadow=True, startangle=90, colors=colors)
    ax1.axis('equal')
    ax1.legend(frameon=True, bbox_to_anchor=(1.6,1))
    ax1.set_title("Répartition du parc de logements visités selon la catégorie", fontsize=20)
    
    plt.show()

    conn.commit()
    cur.close()
    conn.close()   


def graph_dpe_CE():
    conn = sqlite3.connect('./Code/dpe.db')
    cur = conn.cursor()
    
    query = "SELECT [consommation_energie], [classe_estimation_ges], [classe_consommation_energie], [tr002_type_batiment_libelle] FROM dpe_filtré;"
    graph = pd.read_sql(query, conn)
           
    
    n = graph[graph['classe_consommation_energie'] == 'N' ].index
    graph.drop(n, inplace=True)
    cmap = ListedColormap(['#929591', '#7fc97f', '#f0027f'])
    ax = graph.groupby(["classe_consommation_energie", "tr002_type_batiment_libelle"])["tr002_type_batiment_libelle"].size().groupby(level=0).apply(
            (lambda x: (100 * x / x.sum()).round(2))
    ).unstack().plot(kind='bar',stacked=False, figsize=(30,10), ylabel="% de repartition", colormap=cmap, fontsize=20)

    for i, rect in enumerate(ax.patches):
        if rect.get_height()>1:
            ax.text ( rect.get_x() + rect.get_width() / 13,rect.get_height(),"%.1f%%"% rect.get_height(), weight='bold', fontsize=15)
          
    ax.set_xlabel ("Classe DPE", fontsize=20 )
    ax.set_ylabel ("Pourcentage", fontsize=20)
    plt.legend(bbox_to_anchor=(1,1,0,0), fontsize=20)
    ax.set_title("Répartition par étiquette énergie suivant le type du batiment", fontsize=30)
    plt.gca().yaxis.set_major_formatter(ticker.PercentFormatter())
    plt.show()
    
    conn.commit()
    cur.close()
    conn.close()
    

def graph_dpe_ges():
    conn = sqlite3.connect('./Code/dpe.db')
    cur = conn.cursor()
    
    query = "SELECT [consommation_energie], [classe_estimation_ges], [classe_consommation_energie], [tr002_type_batiment_libelle] FROM dpe_filtré;"
    graph = pd.read_sql(query, conn)
    
    n = graph[graph['classe_estimation_ges'] == 'N' ].index
    graph.drop(n, inplace=True)
    cmap = ListedColormap(['#929591', '#7fc97f', '#f0027f'])
    ax = graph.groupby(["classe_estimation_ges","tr002_type_batiment_libelle"])["tr002_type_batiment_libelle"].size().groupby(level=0).apply(
            (lambda x: (100 * x / x.sum()).round(2))
            ).unstack().plot(kind='bar',stacked=False, figsize=(30,10), ylabel="% de repartition", colormap=cmap, fontsize=20)

    for i, rect in enumerate(ax.patches):
        if rect.get_height()>1:
            ax.text ( rect.get_x() + rect.get_width() / 13,rect.get_height(),"%.1f%%"% rect.get_height(), weight='bold', fontsize=15 )
   
    ax.set_xlabel ("Classe GES", fontsize=20 )
    ax.set_ylabel ("Pourcentage", fontsize=20)
    plt.legend(bbox_to_anchor=(1,1,0,0), fontsize=20)
    ax.set_title("Répartition par étiquette climat suivant le type du batiment", fontsize=30)
   
    
    plt.gca().yaxis.set_major_formatter(ticker.PercentFormatter())
    plt.show()
    
    conn.commit()
    cur.close()
    conn.close()


# In[ ]:





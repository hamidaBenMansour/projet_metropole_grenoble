#!/usr/bin/env python
# coding: utf-8

# In[1]:


import geopandas as gpd
import folium 
import sqlite3 as sq
import pandas as pd
from shapely import wkt
from shapely.geometry import Polygon
#gdf = gpd.read_file("Counties_Shoreline.shp")

def carto_m_iris():
    conn = sq.connect('./Code/dpe.db')
    cur = conn.cursor()
    filtre_iris = pd.read_sql('select * from filtre_iris', conn)
    conn.commit()
    conn.close()
    gdf = gpd.GeoDataFrame(filtre_iris, crs="EPSG:4326") 
    gdf = gpd.GeoDataFrame(filtre_iris, crs="EPSG:4326")
    gdf['geometry'] = gdf['geometry'].apply(wkt.loads)
    df_geo = gpd.GeoDataFrame(gdf, geometry = 'geometry')
    df_geo.set_geometry('geometry', inplace=True)
    m = folium.Map(location=[45.13490997993675, 5.7143117808939294], tiles="OpenStreetMap", zoom_start=7)
    fg = folium.FeatureGroup(name="IRIS", show = True)
    m.add_child(fg)
    fg.add_child(folium.GeoJson(data=df_geo["geometry"]))#.add_to(m)
    return(m)


# In[ ]:





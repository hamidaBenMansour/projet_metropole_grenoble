# Documentation technique

## download.py
### download():

Une fonction qui permet de télécharger les données brutes nécessaires qui n'existent pas en base de données.<br>


## load.py
### load():

Une fonction qui permet de charger les données en base de données à partir des fichiers télechargées dans des tables.<br>
cette fonction comprend plusieurs sous-fonctions :<br>
#### load_dpe()
#### load_communes()
#### load_commune_nb_logements()
#### load_contours_iris()
#### load_iris_nb_logements()


## Duplicate_tables.py
### duplicate_tables():

Une fonction qui permet de dupliquer les tables de données brutes qui existent en base de données.<br>

## Corrig_nom_com.py
### corrig_nom_commune()

Une fonction qui permet de corriger les noms de communes mal orthographiés .<br>

## Load_dpe_corrige.py
### load_dpe_corrigé()

Une fonction permet de injecter les données de dpe après la normalisation de la colonne commune .<br>

## formate_Code_insee.py
### formate()

Une fonction permet de normaliser la colonne code insee.<br>

## nom_commune.py
### nom_commune():

Une fonction qui retourne une liste des noms de 49 communes de la Métropole du Grenoble.<br>

##  Filtre_dpe_38.py 
### filtre()

Une fonction qui permet de filtrer la dataset dpe selon les codes insees de 49 communes de la Métropole du Grenoble.<br>

## Taux_Couverture.py
### find_Tc():

Une fonction permet de calculer le taux de couverture par commune.<br>

##  transform_iris_projections.py
### transform_iris_projections():

Une fonction qui permet de transformer les coordonnées lambert en coordonées lat et long.<br>

## fetch_iris.py 
### fetch_iris()

Une fonction qui permet de filtrer la table iris selon le 49 communes de la Métropole.<br>

## Do_iris_geojson.py
### do_iris_geojson():

Une fonction qui permet de convertir la dataframe iris filtré en fichier geojson.<br>

## API_dpe.py
### Download_Api_dpe() 

Une foction permet de mettre à jour les données dpe mais en récupérant seulement les 10000 premiers lignes.<br>










#!/usr/bin/env python
# coding: utf-8

# In[1]:


import sqlite3



def double_tables():
    conn = sqlite3.connect('./Code/dpe.db')
    cur = conn.cursor()

    
    cur.execute("""SELECT name FROM sqlite_master WHERE type='table' AND name='dpe'""")
    try:
        if not cur.fetchone()[0] == "dpe":
            print("La table <dpe> n'existe pas : création de la table <dpe>...")
            cur.execute("""CREATE TABLE dpe AS SELECT * FROM raw_dpe""")
    except TypeError as error:
        name = str(error)
        if not name == "'NoneType' object is not subscriptable":
            raise
        else:
            print("La table <dpe> n'existe pas : création de la table <dpe>...")
            cur.execute("""CREATE TABLE dpe AS SELECT * FROM raw_dpe""")
            
            
    cur.execute("""SELECT name FROM sqlite_master WHERE type='table' AND name='communes'""")
    try:
        if not cur.fetchone()[0] == "communes":
            print("La table <communes> n'existe pas : création de la table <communes>...")
            cur.execute("""CREATE TABLE communes AS SELECT * FROM raw_communes""")
    except TypeError as error:
        name = str(error)
        if not name == "'NoneType' object is not subscriptable":
            raise
        else:
            print("La table <communes> n'existe pas : création de la table <communes>...")
            cur.execute("""CREATE TABLE communes AS SELECT * FROM raw_communes""")
      

    
    cur.execute("""SELECT name FROM sqlite_master WHERE type='table' AND name='iris'""")
    try:
        if not cur.fetchone()[0] == "iris":
            print("La table <iris> n'existe pas : création de la table <iris>...")
            cur.execute("""CREATE TABLE iris AS SELECT * FROM raw_iris""")
    except TypeError as error:
        name = str(error)
        if not name == "'NoneType' object is not subscriptable":
            raise
        else:
            print("La table <iris> n'existe pas : création de la table <iris>...")
            cur.execute("""CREATE TABLE iris AS SELECT * FROM raw_iris""")
            
    conn.commit()
    cur.close()
    conn.close()


# In[3]:


double_tables()


# In[ ]:





# In[ ]:





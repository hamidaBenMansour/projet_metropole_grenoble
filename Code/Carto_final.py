#!/usr/bin/env python
# coding: utf-8

# In[1]:


import pandas as pd
import xlrd
import requests
import folium
import folium.plugins
import geopandas as gpd
import matplotlib.pyplot as plt
import numpy as np
import altair as alt
from geopy.geocoders import Nominatim
from geopy.exc import GeocoderTimedOut 



def carto():
    df=pd.read_csv("dpe_Metropole.csv")
    Logement=pd.read_csv("taux_couverture_commune.csv")
    fname = 'communes.json'
    nil = gpd.read_file(fname)
    nil=nil[['nom', 'code_insee', 'geometry']]
    nil['nom'] = nil['nom'].str.lower()
    Logement['codgeo']=Logement['codgeo'].astype(object)

    Logement=Logement[['codgeo','commune','Taux de couverture(%)', 'Nombre de logements contrôlés', 'Nombre total de logements']]
    Logement.rename(columns={'commune': 'nom'}, inplace=True)
    Logement.rename(columns={'codgeo': 'code_insee'}, inplace=True)
    data_geo=nil.merge(Logement,on=['code_insee']) 
    data_geo=nil.join(Logement.set_index('nom'), on='nom', how='left', lsuffix='_left', rsuffix='_right')
    #data_geo.rename(columns={'nom_right': 'nom'}, inplace=True)
    
    
    
    '''We create another map called mymap'''
    x_map=nil.centroid.x.mean()
    y_map=nil.centroid.y.mean()

    mymap = folium.Map(location=[y_map, x_map], zoom_start=11,tiles='OpenStreetMap')
    folium.TileLayer('OpenStreetMap',name="Light Map",control=False).add_to(mymap)
    myscale = (data_geo['Taux de couverture(%)'].quantile((0,0.1,0.75,0.9,0.98,1))).tolist()
    
#'''Set up Choropleth map'''
    mymap.choropleth(
     geo_data=data_geo,
     name='Choropleth',
     data=data_geo,
     columns=['nom','Taux de couverture(%)'],
     key_on="feature.properties.nom",
     fill_color='Reds',
     threshold_scale=myscale,
     Highlight= True,
     line_color = "#0000",
     show=True,
     overlay=True,
     nan_fill_color = "White",  
     fill_opacity=1,
     line_opacity=0.2,
     legend_name='Taux de couverture de Dpe en % par commune',
     smooth_factor=0)
    
#'''Add hover functionality'''
    style_function = lambda x: {'fillColor': '#ffffff', 
                                'color':'#000000', 
                                'fillOpacity': 0.1, 
                                'weight': 0.1}
    highlight_function = lambda x: {'fillColor': '#000000', 
                                    'color':'#000000', 
                                    'fillOpacity': 0.50, 
                                    'weight': 0.1}

    NIL = folium.features.GeoJson(data_geo,
        style_function=style_function, 
        control=False,
        highlight_function=highlight_function, 
        tooltip=folium.features.GeoJsonTooltip(
            fields=['nom','Taux de couverture(%)'],
            aliases=['nom_commune: ','Taux de couverture en %: '],
            style=("background-color: white; color: #333333; font-family: arial; font-size: 12px; padding: 10px;")))
    

    mymap.add_child(NIL)
       
#'''Add the geocoded locations to the map'''
    geolocator = Nominatim(user_agent="cartographie-pav")
    
    #'''Add dark and light mode'''
    folium.TileLayer('cartodbdark_matter',name="Dark mode",control=True).add_to(mymap)
    folium.TileLayer('cartodbpositron',name="Light mode",control=True).add_to(mymap)
    Layer1 = folium.FeatureGroup(name="Taux de répartition", show=True).add_to(mymap)
    Layer2 = folium.FeatureGroup(name="Taux de couverture", show=True).add_to(mymap)
    
    for coord,code,lib in zip(data_geo["geometry"],data_geo["code_insee_left"],data_geo["nom"]):
        
        geoloc = folium.GeoJson(data=coord)
        dpe_ce = df.groupby(["code_insee_commune","classe_consommation_energie"])["classe_consommation_energie"].size().groupby(level=0).apply(lambda x: 100 * x / x.sum()).round(2)
        dpe_ges = df.groupby(["code_insee_commune","classe_estimation_ges"])["classe_estimation_ges"].size().groupby(level=0).apply(lambda x: 100 * x / x.sum()).round(2)
        
        ges = dpe_ges[code].tolist()
        ce = dpe_ce[code].tolist()
        ce.pop()
        ges.pop()
        index_ce = dpe_ce[code].index.tolist()
        index_ges = dpe_ges[code].index.tolist()
        
        index_ce.pop()
        index_ges.pop()
        centre1 = geolocator.geocode(f'{lib}, isère', addressdetails=True)
        
        source_dpe = pd.DataFrame(
            {'classe': index_ce, 'Pourcentage': ce,})
        
        source_ges = pd.DataFrame(
            {'classe': index_ges, 'Pourcentage': ges,}) 
        
    #.size().groupby(level=0).apply(lambda x: 100 * x / x.sum())
        
    
    # create an altair chart, then convert to JSON
        chart_ce = alt.Chart(source_dpe).mark_bar(filled=True).encode(
            x=alt.X('classe:O', axis=alt.Axis(title='classe')),
            y=alt.Y('Pourcentage:Q', axis=alt.Axis(title='Pourcentage(%) DPE')),
            color='classe', tooltip = [alt.Tooltip('Pourcentage')]
            ).interactive()
        chart_ges = alt.Chart(source_ges).mark_bar(filled=True).encode(
            x=alt.X('classe:O', axis=alt.Axis(title='classe')),
            y=alt.Y('Pourcentage:Q', axis=alt.Axis(title='Pourcentage(%) GES')),
            color='classe', tooltip = [alt.Tooltip('Pourcentage')]
            ).interactive() 
        
        chart = alt.hconcat(chart_ce, chart_ges)
        
        vis1 = chart.to_json()
              
        geoloc.add_child(folium.Popup(max_width=500).add_child(folium.VegaLite(vis1, width=500, height=200)))
        geoloc.add_to(Layer1)
    
    
    map_interractive = folium.features.GeoJson(
    data_geo,
    style_function=style_function, 
    control=False,
    highlight_function=highlight_function, 
    tooltip=folium.features.GeoJsonTooltip(
        fields=['nom','code_insee_left', 'Nombre de logements contrôlés', 'Nombre total de logements', 'Taux de couverture(%)'],
        aliases=['Commune: ', 'Code insee: ',  'Logements contrôlés: ', 'Total de logements en 2017: ', 'Taux de couverture en %: '],
        style=("background-color: white; color: #333333; font-family: arial; font-size: 12px; padding: 10px;"),
        )
    )

    Layer2.add_child(map_interractive)
   
    

#'''We add a layer controller'''
    folium.LayerControl(collapsed=False).add_to(mymap)
    return mymap


#!/usr/bin/env python
# coding: utf-8

# In[1]:


import sqlite3
import pandas

def load_dpe_filtré():
    """
    injecter dpe_38 data dans la base de données
    
    """
    conn = sqlite3.connect("./Code/dpe.db")
    cur = conn.cursor()
    with open('dpe_Metropole.csv') as dpe_file:
        dpe_df = pandas.read_csv(dpe_file)
    cur.execute("""DROP TABLE IF EXISTS dpe_filtré""")
    dpe_df.to_sql("dpe_filtré", conn)
    conn.commit()
    cur.close()
    conn.close()
    print("Fin du chargement des données brutes de dpe en base de données")


# In[ ]:





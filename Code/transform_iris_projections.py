#!/usr/bin/env python
# coding: utf-8

# In[ ]:


import geopandas as gpd
import shapely

def transform_iris_projections():
    row_iris = gpd.read_file("IRIS-GE_2-0__SHP_LAMB93_D038_2020-01-01/IRIS-GE/"         "1_DONNEES_LIVRAISON_2020-07-00352/IRIS-GE_2-0_SHP_LAMB93_D038-2020/IRIS_GE.SHP")
#print (row_iris.crs)
    iris_lat_long = row_iris.copy()
    iris_lat_long= iris_lat_long.to_crs({'init': 'epsg:4326'})
#print (iris_lat_long.crs)
    return (iris_lat_long)
print ('la transformation des coordonnées est terminée')


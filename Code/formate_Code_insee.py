#!/usr/bin/env python
# coding: utf-8

# In[1]:


import re
import sqlite3

def formate():
    conn = sqlite3.connect('./Code/dpe.db')
    cur = conn.cursor()

    cur.execute(f"""SELECT DISTINCT code_insee_commune FROM dpe WHERE code_insee_commune LIKE '%.0%'""")
    results = cur.fetchall()

    for result in results:
        search_result = re.search(r"\.\d+$", result[0])
        if search_result:
            point_et_decimales = search_result.group()
            new = result[0][:len(result)-len(point_et_decimales)]
            cur.execute(f'''UPDATE dpe SET code_insee_commune = "{new}" WHERE code_insee_commune = "{result[0]}"''')

    conn.commit()
    cur.close()
    conn.close()


# In[ ]:





#!/usr/bin/env python
# coding: utf-8

# In[1]:


import pandas as pd
import sqlite3 as sq
import shapely

try:
    import  transform_iris_projections
    
except ModuleNotFoundError:
    
    from Code import transform_iris_projections
    
    
iris_lat_long=transform_iris_projections.transform_iris_projections()

def load_iris_lat_long ():
    
    iris_lat_long['geometry'] = iris_lat_long.apply(lambda x: shapely.wkt.dumps(x.geometry), axis=1)
    iris_lat_long.rename(columns={'INSEE_COM': 'code_insee', 'NOM_COM': 'nom_commune', 
                     'IRIS': 'iris', 'CODE_IRIS': 'code_iris', 
                     'NOM_IRIS': 'nom_iris', 'TYP_IRIS': 'type_iris'  }, inplace=True)
    conn = sq.connect('./Code/dpe.db')
    cur = conn.cursor()
    cur.execute('''DROP TABLE IF EXISTS clean_iris''')
    iris_lat_long.to_sql('clean_iris', conn, if_exists='replace', index=False) # - writes the pd.df to SQLIte DB

    conn.commit()
    conn.close()
    print("Fin du chargement des données brutes des contours iris_lat_long transformés en base de données")


# In[ ]:





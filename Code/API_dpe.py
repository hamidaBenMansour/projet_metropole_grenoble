#!/usr/bin/env python
# coding: utf-8

# In[5]:


import json
import requests
import pandas as pd

def Download_Api_dpe():
    params = (
        ('page', '1'),
        ('format', 'json'),
        ('sort', '_score,-date_reception_dpe'),
        ('size', '1000'),
        ('q', 'date_reception_dpe:"2020-11-*"')
    )

    apo=json.loads(
                requests.get(
                    'https://koumoul.com/s/data-fair/api/v1/datasets/dpe-38/lines',
                    params=params
                ).text
            )
    print(
        json.dumps(apo,
            indent=4,
            sort_keys=True
        )
    )

    df = pd.DataFrame()
    temp = pd.DataFrame()
    temp = pd.DataFrame(apo['results'])
    df = pd.concat([df, temp], axis=1)
    df.to_csv("dpe_API.csv")


# In[ ]:





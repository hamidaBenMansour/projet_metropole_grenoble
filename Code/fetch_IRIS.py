#!/usr/bin/env python
# coding: utf-8

# In[3]:


import csv
import json
import sqlite3
import pandas as pd
import shapely


def fetch_iris():
    """filtrer IRIS table 
    """
    iris_table = None
    conn = sqlite3.connect('./Code/dpe.db')
    cur = conn.cursor()
    cur.execute("""DROP TABLE IF EXISTS filtre_iris""")
    cur.execute(
        """SELECT clean_iris.geometry, clean_iris.code_insee, clean_iris.nom_commune, clean_iris.iris, clean_iris.code_iris, clean_iris.nom_iris
        FROM clean_iris
        JOIN communes
        ON communes.code_insee = clean_iris.code_insee
        WHERE geometry IS NOT NULL"""
    )
    iris_table = [res[0:6] for res in cur.fetchall()]
    df_iris_clean = pd.DataFrame(iris_table, columns = ['geometry' , 'code_insee', 'nom_commune', 'iris', 'code_iris', 'nom_iris']) 
    df_iris_clean.to_sql('filtre_iris', conn, if_exists='replace', index=False) # writes the pd.df to SQLIte DB
 
    
    conn.commit()
    cur.close()
    conn.close()
    return (df_iris_clean)


# In[ ]:





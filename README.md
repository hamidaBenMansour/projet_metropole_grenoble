## Représentation des Diagnostics de performance énergétique (DPE) sur l’Agglomération grenobloise


### Téchnologies utilisées

- Python, sqlite3, Git, GitLab, Jupyter Notebook

- Librairies Python : Altair, requests, py7zr, gzip, pandas, folium, matplotlib, shapely, xlrd

### Description du contenu du dépôt GitLab

Dans ce dépôt on trouvera :

##### Dossiers

- code/ : contient les composants du logiciel.

- Gestion du projet/ : contenant les fichiers à propos de la gestion du projet.

- datasets/ : contient les differents fichiers téléchargés tout au long du pipeline (plats de faible taille et longs au 

traitement) et notamment le csv livrable pour la plateforme (dpe_Metropole.csv).  

- documentation/ : est explicite (contenant quelques fichiers de notes et un fichier readme.md).

- img/ : contient quelques images.


##### Fichiers


- Un fichier .gitignore : contient les fichiers et dossiers générés par le logiciel et dont la présence sur le dépôt Git ne 

présente pas d'intérêt

- Un fichier .gitlab-ci.yml : contient la configuration pour le déploiement des résultats visuels.

- Deux fichier Pipfile & Pipfile.lock : contiennent la configuration de l'environnement Python virtuel utilisé pour le projet.

- Un fichier README.md : est ce fichier / explicite

- Un fichier deploiement.ipynb : c'est un Jupyter Notebook qui contient les visualisations prêtes au déploiement

- Un fichier main.py : orchestre l'exécution des différents fichiers présents dans ce dossier: Code/

- Un fichier docker-compose.yml avec la configuration du container docker postgreSQL (mais je l'ai pas utilisé pour le projet).


### Description du projet

le script main.py fait appel aux fonctions des modules pour :

1. Télecharger l'ensemble des datasets necessaires pour le projet 

2. Injecter les datasets téléchargées dans des tables sqlite3.

3. Dupliquer les tables de données brutes qui existent en base de données.

4. Récuperer une liste des noms de communes depuis la dataset (geojson) de 49 communes de la Métropole du Grenoble.

5. Formater les codes insees de la dataset dpe.

6. Corriger les noms de communes dans le jeu de donnée de source.

7. Transformer les coordonnées iris de type Lambert 93 en coordonnées EPSG 4326 (lon, lat).

8. Injecter la nouvelle dataset dpe dans la base de données.

9. Filtrer la dataset dpe  selon les codes insees de 49 communes de la Métropole du Grenoble.

10. Filtrer les données iris selon les codes insees de 49 communes de la Métropole du Grenoble et l'injecter dans une table 

sqlite3.

11. Injecter la nouvelle dataset dpe_Metropole dans la base de données.

10. Calculer le taux de couverture par commune.

11. Convertir la dataframe iris filtré en fichier geojson.

12. Visualiser la Répartition du parc de logements visités selon la catégorie via un 'Pie chart'

13. Visualiser la répartition par étiquette énergie suivant le type du batiment

14. Visualiser la répartition par étiquette Climat(GES) suivant le type du batiment

15. Visualiser le taux de couverture de l’existence d’un DPE et le taux de répartition des différents niveaux de DPE de 

l’ensemble des habitats par commune via  une cartographie choroplèthe sur fond OpenStreetMap.

16. Visualiser par bâtiment, la catégorie du DPE associée via une cartographie cluster sur fond OpenStreetMap.

17. Visualiser la proportion des classes de consommation énergétique par classe d'estimation d'émissions de gaz à effet de serre


Ce script main.py est intégré en continu à l'aide de GitLab Pages.

#### Pipeline

<img src="/Img/pip.png" alt="Pipeline du projet">

### Gitlab pages

    https://hamidabenmansour.gitlab.io/projet_metropole_grenoble/  
         
Ce lien vous emmène sur "mon" site crée à partir de gitlab pages.

Vous y trouverez des representation cartographique (une choroplèthe et un carto cluster) et  des graphiques. Aussi , vous y

trouverez quelques explications associées à ces visualistions pour les comprendre.


### Bibliothèque

#### Rendu final 

    https://docs.google.com/presentation/d/1nY8q9CsdW9bFe-WlWTezrwg2R-_600YNzwF4vUa9MoU/edit#slide=id.gd847b120e8_0_7

Ce lien vous emmène vers ma présentation powerpoint.


#### Sites utilisés pour mon Jupyter statique en ligne

- Jeu de données des diagnostics de performance énergétique des logements d'Isère
    
    https://data.ademe.fr/datasets/dpe-38

- Jeu de données de l'opendata de la métropole grenobloise listant les communes de l'intercommunalité Grenoble-Alpes-Auvergne:

    https://data.metropolegrenoble.fr/ckan/dataset/les-communes-de-la-metropole

- Jeux de données de l'IGN dont le jeu avec les contours IRIS:

    https://geoservices.ign.fr/documentation/diffusion/telechargement-donnees-libres.html#irisge


- License opendata
    
    https://api.gouv.fr/les-api/api_dpe_batiments_publics


- Nombre de logements 2017
    https://www.data.gouv.fr/fr/datasets/logement/

    Vers
    https://www.insee.fr/fr/statistiques

    Vers
    https://www.insee.fr/fr/information/3544265

    Vers
    https://statistiques-locales.insee.fr/#c=report&chapter=compar&report=r01&selgeo1=com_courant.38185


- Documentation API

    https://koumoul.com/en/documentation/developer 


- Recherche d'informations (techniques)  
    https://stackoverflow.com/

    htps://doc.ubuntu-fr.org/sqlite

    https://plotly.com

    https://waytolearnx.com/2019/04/comment-extraire-un-fichier-zip-en-python

    htmlhttps://moonbooks.org/Articles/Comment-lire-un-fichier-excel-extension-xlsx-avec-pandas-en-python-/

    https://docs.python.org/3/library/urllib.request.html#examples 

    https://www.kaggle.com/

    https://pypi.org/


####  Librairie python utilisées

Pandas : https://pandas.pydata.org/

urllib.request : https://docs.python.org/3/library/urllib.request.html

Matplotlib : https://matplotlib.org/stable/index.html

Geopandas : https://geopandas.org/

Seaborn : https://seaborn.pydata.org/

Altair : https://altair-viz.github.io/

gzip : https://docs.python.org/3/library/gzip.html

Folium : https://python-visualization.github.io/folium/

Py7zr : https://pypi.org/project/py7zr/

Shapely : https://shapely.readthedocs.io/en/stable/manual.html

Xlrd : https://pypi.org/project/xlrd/


****Avant de pouvoir commencer à explorer le projet, il est nécessaire de suivre les étapes suivantes:****

### Installation de l'environement virtuel

Installer l'environnement virtuel à l'aide de la commande suivante :

```
pipenv install 

```
### Lancement de l'environement virtuel

Lancer l'environement virtuel à l'aide de la commande suivante :

```
pipenv shell

```

### Lancement de jupyter Notebook

```
jupyter notebook

```
Une fois dans le jupyter notebook, lancez le fichier nommé deploiment.ipynb.
Exécutez les cellules une par une avec la commande suivante :

```
shift + entrée

```
### Mise à jour de la page déployée

Une fois vous modifiez le contenu d'une ou plusieurs cellule dans le fichier deploiment.ipynb. Alors pour mettre à jour la page déployée, éxécutez les commandes suivantes par ordre :

```
git add deploiment.ipynb

```

```
git commit -m 'deploiment.ipynb'

```

```
git push origin master

```
Une fois vous pushez sur gitlab, vous allez attendre environ 7 minutes pour que la page puisse se déployer.





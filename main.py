#!/usr/bin/env python
# coding: utf-8

# In[ ]:


from Code import Download
from Code import Load
from Code import Duplicate_tables
from Code import nom_commune
from Code import Corrig_nom_com
from Code import Load_dpe_corrige
from Code import load_dpe_filtre
from Code import transform_iris_projections
from Code import formate_Code_insee
from Code import fetch_IRIS 
from Code import Taux_couverture
from Code import Do_IRIS_Geojson



Download.download_dpe()
Download.download_json()
Download.download_cont_iris()
Download.download_log_communes()
Download.download_log_IRIS()

Load.load_dpe()
Load.load_communes()
Load.load_iris_nb_logements()
Load.load_contours_iris()

Duplicate_tables.double_tables()


Corrig_nom_com.corrig_nom_commune()
formate_Code_insee.formate()
transform_iris_projections.transform_iris_projections()


Load_dpe_corrige.load_dpe_corrigé()
nom_commune.nom_commune()
load_dpe_filtre.load_dpe_filtré()

Taux_couverture.find_Tc()

fetch_IRIS.fetch_iris()
Do_IRIS_Geojson.do_iris_geojson()


# In[ ]:





### Compte des valeurs nulles pour la data dpe de la métropole de Grenoble
Nombre de lignes :
62459

Nombre de valeurs manquantes dans la colonne id :
0

Nombre de valeurs manquantes dans la colonne numero_dpe  :
0

Nombre de valeurs manquantes dans la colonne consommation_energie :
0

Nombre de valeurs manquantes dans la colonne classe_consommation_energie :
0

Nombre de valeurs manquantes dans la colonne estimation_ges :
0

Nombre de valeurs manquantes dans la colonne classe_estimation_ges :
0

Nombre de valeurs manquantes dans la colonne secteur_activite:
57985

Nombre de valeurs manquantes dans la colonne  type_voie:
41283

Nombre de valeurs manquantes dans la colonne nom_rue :
4

Nombre de valeurs manquantes dans la colonne numero_rue :
42887

Nombre de valeurs manquantes dans la colonne batiment :
60427

Nombre de valeurs manquantes dans la colonne etage :
56533

Nombre de valeurs manquantes dans la colonne code_insee_commune :
0

Nombre de valeurs manquantes dans la colonne nombre_niveaux :
15996

Nombre de valeurs manquantes dans la colonne longitude :
5229

Nombre de valeurs manquantes dans la colonne latitude :
5229

Nombre de valeurs manquantes dans la colonne geo_adresse :
5229

Nombre de valeurs manquantes dans la colonne geo_id :
5644

### Compte des valeurs nulles pour le jeux de données de base
190969 lignes en tout pour la table dpe.

0 valeurs nulles  pour la colonne id.

0 valeurs nulles pour la colonne numero_dpe.

0 valeurs nulles pour la colonne usr_diagnostiqueur_id.

0 valeurs nulles trouvées pour la colonne usr_logiciel_id.

0 valeurs nulles trouvées pour la colonne tr001_modele_dpe_id.

0 valeurs nulles trouvées pour la colonne nom_methode_dpe.

38898 valeurs nulles trouvées pour la colonne version_methode_dpe.

161129 valeurs nulles trouvées pour la colonne nom_methode_etude_thermique.

163041 valeurs nulles trouvées pour la colonne version_methode_etude_thermique.

0 valeurs nulles trouvées pour la colonne date_visite_diagnostiqueur.

0 valeurs nulles trouvées pour la colonne date_etablissement_dpe.

0 valeurs nulles trouvées pour la colonne date_arrete_tarifs_energies.

157229 valeurs nulles trouvées pour la colonne commentaires_ameliorations_recommandations.

157676 valeurs nulles trouvées pour la colonne explication_personnalisee.

0 valeurs nulles trouvées pour la colonne consommation_energie.

0 valeurs nulles trouvées pour la colonne classe_consommation_energie.

0 valeurs nulles trouvées pour la colonne estimation_ges.

0 valeurs nulles trouvées pour la colonne classe_estimation_ges.

0 valeurs nulles trouvées pour la colonne tr002_type_batiment_id.

175547 valeurs nulles trouvées pour la colonne secteur_activite.

187876 valeurs nulles trouvées pour la colonne tr012_categorie_erp_id.

187388 valeurs nulles trouvées pour la colonne tr013_type_erp_id.

0 valeurs nulles trouvées pour la colonne annee_construction.

3 valeurs nulles trouvées pour la colonne surface_habitable.

3 valeurs nulles trouvées pour la colonne surface_thermique_lot.

0 valeurs nulles trouvées pour la colonne tv016_departement_id.

45 valeurs nulles trouvées pour la colonne commune.

112733 valeurs nulles trouvées pour la colonne arrondissement.

147806 valeurs nulles trouvées pour la colonne type_voie.

94 valeurs nulles trouvées pour la colonne nom_rue.

148301 valeurs nulles trouvées pour la colonne numero_rue.

182347 valeurs nulles trouvées pour la colonne batiment.

142475 valeurs nulles trouvées pour la colonne escalier.

172827 valeurs nulles trouvées pour la colonne etage.

184765 valeurs nulles trouvées pour la colonne porte.

45 valeurs nulles trouvées pour la colonne code_postal.

45 valeurs nulles trouvées pour la colonne code_insee_commune.

45 valeurs nulles trouvées pour la colonne code_insee_commune_actualise.

99650 valeurs nulles trouvées pour la colonne numero_lot.

184218 valeurs nulles trouvées pour la colonne quote_part.

190967 valeurs nulles trouvées pour la colonne nom_centre_commercial.

169980 valeurs nulles trouvées pour la colonne surface_commerciale_contractuelle.

39492 valeurs nulles trouvées pour la colonne portee_dpe_batiment.

190962 valeurs nulles trouvées pour la colonne partie_batiment.

43557 valeurs nulles trouvées pour la colonne shon.

39492 valeurs nulles trouvées pour la colonne surface_utile.

39492 valeurs nulles pour la colonne surface_thermique_parties_communes.

43557 valeurs nulles  pour la colonne en_souterrain.

43557 valeurs nulles  pour la colonne en_surface.

39492 valeurs nulles pour la colonne nombre_niveaux.

43557 valeurs nulles  pour la colonne nombre_circulations_verticales.

43557 valeurs nulles pour la colonne nombre_boutiques.

39492 valeurs nulles pour la colonne presence_verriere.

43557 valeurs nulles  pour la colonne surface_verriere.

190969 valeurs nulles pour la colonne type_vitrage_verriere.

43557 valeurs nulles pour la colonne nombre_entrees_avec_sas.

43552 valeurs nulles pour la colonne nombre_entrees_sans_sas.

41233 valeurs nulles trouvées pour la colonne surface_baies_orientees_nord.

41233 valeurs nulles trouvées pour la colonne surface_baies_orientees_est_ouest.

41233 valeurs nulles trouvées pour la colonne surface_baies_orientees_sud.

41233 valeurs nulles trouvées pour la colonne surface_planchers_hauts_deperditifs.

41233 valeurs nulles trouvées pour la colonne surface_planchers_bas_deperditifs.

41233 valeurs nulles trouvées pour la colonne surface_parois_verticales_opaques_deperditives.

0 valeurs nulles trouvées pour la colonne etat_avancement.

365 valeurs nulles trouvées pour la colonne organisme_certificateur.

3348 valeurs nulles trouvées pour la colonne adresse_organisme_certificateur.

18963 valeurs nulles trouvées pour la colonne dpe_vierge.

0 valeurs nulles trouvées pour la colonne est_efface.

0 valeurs nulles trouvées pour la colonne date_reception_dpe.

27716 valeurs nulles trouvées pour la colonne longitude.

27716 valeurs nulles trouvées pour la colonne latitude.

0 valeurs nulles trouvées pour la colonne geo_score.

27716 valeurs nulles trouvées pour la colonne geo_type.

27716 valeurs nulles trouvées pour la colonne geo_adresse.

29137 valeurs nulles trouvées pour la colonne geo_id.

40191 valeurs nulles trouvées pour la colonne geo_l4.

189071 valeurs nulles trouvées pour la colonne geo_l5.

0 valeurs nulles trouvées pour la colonne tr001_modele_dpe_code.

0 valeurs nulles trouvées pour la colonne tr001_modele_dpe_type_id.

0 valeurs nulles trouvées pour la colonne tr001_modele_dpe_modele.

0 valeurs nulles trouvées pour la colonne tr001_modele_dpe_description.

0 valeurs nulles trouvées pour la colonne tr001_modele_dpe_fichier_vierge.

0 valeurs nulles trouvées pour la colonne tr001_modele_dpe_est_efface.

0 valeurs nulles trouvées pour la colonne tr001_modele_dpe_type.

0 valeurs nulles trouvées pour la colonne tr001_modele_dpe_type_libelle.

0 valeurs nulles trouvées pour la colonne tr001_modele_dpe_type_ordre.

0 valeurs nulles trouvées pour la colonne tr002_type_batiment_code.

0 valeurs nulles trouvées pour la colonne tr002_type_batiment_description.

0 valeurs nulles trouvées pour la colonne tr002_type_batiment_libelle.

0 valeurs nulles trouvées pour la colonne tr002_type_batiment_est_efface.

0 valeurs nulles trouvées pour la colonne tr002_type_batiment_ordre.

0 valeurs nulles trouvées pour la colonne tr002_type_batiment_simulateur.

187876 valeurs nulles trouvées pour la colonne tr012_categorie_erp_code.

187876 valeurs nulles trouvées pour la colonne tr012_categorie_erp_categorie.

187876 valeurs nulles trouvées pour la colonne tr012_categorie_erp_groupe.

187876 valeurs nulles trouvées pour la colonne tr012_categorie_erp_est_efface.

187388 valeurs nulles trouvées pour la colonne tr013_type_erp_code.

187388 valeurs nulles trouvées pour la colonne tr013_type_erp_type.

187388 valeurs nulles trouvées pour la colonne tr013_type_erp_categorie_id.

187388 valeurs nulles trouvées pour la colonne tr013_type_erp_est_efface.

187388 valeurs nulles trouvées pour la colonne tr013_type_erp_categorie.

0 valeurs nulles trouvées pour la colonne tv016_departement_code.

0 valeurs nulles trouvées pour la colonne tv016_departement_departement.

0 valeurs nulles trouvées pour la colonne tv017_zone_hiver_id.

0 valeurs nulles trouvées pour la colonne tv018_zone_ete_id.

0 valeurs nulles trouvées pour la colonne tv016_departement_altmin.

0 valeurs nulles trouvées pour la colonne tv016_departement_altmax.

0 valeurs nulles trouvées pour la colonne tv016_departement_nref.

0 valeurs nulles trouvées pour la colonne tv016_departement_dhref.

0 valeurs nulles trouvées pour la colonne tv016_departement_pref.

0 valeurs nulles trouvées pour la colonne tv016_departement_c2.

0 valeurs nulles trouvées pour la colonne tv016_departement_c3.

190969 valeurs nulles trouvées pour la colonne tv016_departement_c4.

0 valeurs nulles trouvées pour la colonne tv016_departement_t_ext_basse.

0 valeurs nulles trouvées pour la colonne tv016_departement_e.

0 valeurs nulles trouvées pour la colonne tv016_departement_fch.

0 valeurs nulles trouvées pour la colonne tv016_departement_fecs_ancienne_m_i.

0 valeurs nulles trouvées pour la colonne tv016_departement_fecs_recente_m_i.

0 valeurs nulles trouvées pour la colonne tv016_departement_fecs_solaire_m_i.

0 valeurs nulles trouvées pour la colonne tv016_departement_fecs_ancienne_i_c.

0 valeurs nulles trouvées pour la colonne tv016_departement_fecs_recente_i_c.

0 valeurs nulles trouvées pour la colonne tv017_zone_hiver_code.

0 valeurs nulles trouvées pour la colonne tv017_zone_hiver_t_ext_moyen.

0 valeurs nulles trouvées pour la colonne tv017_zone_hiver_peta_cw.

0 valeurs nulles trouvées pour la colonne tv017_zone_hiver_dh14.

0 valeurs nulles trouvées pour la colonne tv017_zone_hiver_prs1.

0 valeurs nulles trouvées pour la colonne tv018_zone_ete_code.

0 valeurs nulles trouvées pour la colonne tv018_zone_ete_sclim_inf_150.

0 valeurs nulles trouvées pour la colonne tv018_zone_ete_sclim_sup_150.

0 valeurs nulles trouvées pour la colonne tv018_zone_ete_rclim_autres_etages.

0 valeurs nulles trouvées pour la colonne tv018_zone_ete_rclim_dernier_etage.



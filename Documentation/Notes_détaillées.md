** Pour récuperer les codes IRIS, il faut:

- télécharger les données avec requests et urllib: "https://docs.python.org/3/library/urllib.request.html#examples"

- les extraires (elles sont zippées) avec py7zr par exemple: "Comment décompresser le fichier (contours_iris.7z)
https://github.com/miurahr/py7zr"

-  utiliser un module pour pouvoir les lire (fiona par exemple)

** Une fois que c'est extrait tu auras un dossier et les données sont rangées là 

IRIS-GE_2-0__SHP_LAMB93_D038_2020-01-01/IRIS-GE/" \"1_DONNEES_LIVRAISON_2020-07-00352/IRIS-GE_2-0_SHP_LAMB93_D038-2020/IRIS_GE.SHP

** IRIS_GE.SHP ça ça s'utilise facilement avec fiona


```python

```

Le script python main.py appelle les autres:

Le script python Download.py télécharge les différents jeux de données qui ne sont pas présents en local.

Le script python load.py injecte les jeux de données bruts dans la base de données (dpe.db (sqlite3)).

Le script python Duplicate_Tables.py duplique les tables de données brutes dans des tables.

Le script Python formate_code_insee_dpe.py transforme les codes insees de type float en integer pour augmenter la cohérence des données.

Le script python Filtre_dpe_38.py filtre le jeu de données bruts selon la liste des codes insees de 49 communes de la Métropole de grenoble. 

Le script Python transform_iris_projections.py sert à transformer les coordonnées iris de type Lambert 93 en coordonnées classiques EPSG 4326 (lon, lat).



```python

```

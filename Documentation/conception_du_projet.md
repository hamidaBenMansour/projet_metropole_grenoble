# Conception du projet

**********


## Une Vision globale 

Ce projet concerne le diagnostic de performance énergétique du Métropole de Grenoble que nous souhaitons analyser et visualiser.
Pour mieux répondre à ça il faudra : 

***prendre connaissance des données disponibles (client, l'ADEME) et extraire des KPIs et des coordonnées géographiques.

***documenter.

***la mise à jour des données puisse être faite automatiquement et que la mise à jour soit répercutée sur tout le processus de traitement de la donnée.



**********

## Objectifs souhaités

* Filtrer les données de l'isére pour obtenir celles de la Métropole (49 communes).
* Comprendre et explorer les données.
* Obtenir les données du nombre de logements par commune de la Métropole
* Obtenir les données du nombre de logements par maille IRIS de la Métropole
* Enrechir les données si besoin (geocodage)
* Faire des visualisations de type cartographie.
* Faire des visualisations de type graphiques à indicateurs.




**********


```python

```
